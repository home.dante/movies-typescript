import { countries } from "./mock_data/countries";
import { words } from "./mock_data/words";
import { Genre } from "./models/genre";
import { Movie } from "./models/movie";
const number_of_movies: number = 500 * 1000;
const same_rating: boolean = false;
function generateMovies(amount): Array<Movie> {
  let movies: Array<Movie> = [];
  let wSize: number = words.length - 1;
  let cSize: number = countries.length - 1;
  let gSize: number = Genre.length - 1;

  for (let i = 0; i <= amount; i++) {
    movies.push({
      title:
        capitalize(words[generateRandomNumber(0, wSize)]) +
        " " +
        capitalize(words[generateRandomNumber(0, wSize)]) +
        " " +
        capitalize(words[generateRandomNumber(0, wSize)]),

        rating: same_rating ? 8 : generateRandomNumber(10, 100) / 10,
      // rating: generateRandomNumber(1, 10),
      country: countries[generateRandomNumber(0, cSize)].code,
      genre: Genre[generateRandomNumber(0, gSize)],
      ID: i
    });
  }
  // console.log(movies);
  // console.log(words[0]);
  return movies;
}

function generateRandomNumber(min, max) {
  let random = Math.round(Math.random() * (max - min) + min);
  return random;
}
function capitalize(string: string): string {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function deleteMovies(movies: Array<Movie>): void {
  let bestRating: Number = 1;
  let worstRating: Number = 10;
  let worstMovies: Array<Movie> = [];
  let bestMovies: Array<Movie> = [];

  movies.forEach((element: Movie, index, array) => {
    if (element && element.rating <= worstRating) {
      if (element.rating !== worstRating) {
        worstRating = element.rating;
        worstMovies = [];
      }
      worstMovies.push(element);
      array[index]=null;
    }
     if (element && element.rating >= bestRating) {
      if (element.rating !== bestRating) {
        bestRating = element.rating;
        bestMovies = [];
      }
      bestMovies.push(element);
      array[index]=null;
    }
    if(!bestMovies.length)
    bestMovies=worstMovies;
  
  });
  // console.log("--------------------------")
  // console.log("Worst movies: ", worstMovies);
  // console.log("--------------------------")
  // console.log("Best movies: ", bestMovies);
  // console.log("--------------------------")
  // console.log("Filtered movies: ", movies);
//   console.log("--------------------------")
// movies.forEach((element)=>{
//   console.log(element);
// })
}

function highestRatedForCountryByGenre(movies: Array<Movie>) {
  let countryMovieList = {};
  movies.forEach((element) => {
    let country = "" + element.country;
    let genre = "" + element.genre;
    if (!countryMovieList[country]) {
      countryMovieList[country]={}
      Genre.forEach(element => {
        countryMovieList[country][element] = { rating:0, movie:""}
      });
    }
    if(countryMovieList[country][genre].rating<=element.rating){
      countryMovieList[country][genre] = { rating: element.rating, movie:element.title}
    }
  });
  // console.log("--------------------------")
  // console.log(countryMovieList);
}

// Execute functions
let movies = generateMovies(number_of_movies);
let t2 = new Date().getTime();
highestRatedForCountryByGenre(movies)
let t3 = new Date().getTime();

let t0 = new Date().getTime();
deleteMovies(movies);  
let t1 = new Date().getTime();
console.log("Call to highestRatedForCountryByGenre() took " + (t3 - t2) + " milliseconds.")
console.log("Call to deleteMovies() took " + (t1 - t0) + " milliseconds.")