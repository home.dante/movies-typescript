import { Genre } from "./genre";

export class Movie {
  ID: Number;
  title: String;
  rating: Number;
  country: String;
  genre: String;
}
